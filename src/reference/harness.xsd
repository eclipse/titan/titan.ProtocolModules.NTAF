<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:h="http://ntaforum.org/2011/harness"
xmlns:xml="http://www.w3.org/XML/1998/namespace"
targetNamespace="http://ntaforum.org/2011/harness"
elementFormDefault="qualified"
attributeFormDefault="unqualified">
	<xs:import namespace="http://www.w3.org/XML/1998/namespace" schemaLocation="xml-1998.xsd"/>
	<xs:element name="harness" type="h:harness"/>
	<xs:complexType name="harness">
		<xs:sequence>
			<xs:element name="supportedMode" type="h:sessionMode" maxOccurs="unbounded"/>
		</xs:sequence>
		<xs:attribute name="name" type="xs:string" use="required"/>
	</xs:complexType>
	<xs:element name="query-harness">
		<xs:annotation>
			<xs:documentation>Information about a certain harness including descriptions of the
harness itself as well as the actions, events, and nested harnesses it
supports.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:complexContent>
				<xs:extension base="h:harnessDecl">
					<xs:sequence>
						<xs:element name="subharness" minOccurs="0" maxOccurs="unbounded"
type="h:namespace"/>
					</xs:sequence>
					<xs:attribute name="harness" type="h:namespace" use="required"/>
				</xs:extension>
			</xs:complexContent>
		</xs:complexType>
	</xs:element>
	<xs:element name="open">
		<xs:annotation>
			<xs:documentation>A request to open a new session using a specified harness and a set
of parameters and other information consistent with the contract defined in query-
harness</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:complexContent>
				<xs:extension base="h:requestType">
					<xs:sequence>
						<xs:element name="timestamp" type="xs:dateTime" minOccurs="0"/>
						<xs:element name="activationRef" type="xs:string"/>
					</xs:sequence>
					<xs:attribute name="harness" type="h:namespace" use="required"/>
					<xs:attribute name="mode" type="h:sessionMode" use="required"/>
					<xs:attribute name="reportUserActivity" type="xs:boolean" use="optional"
default="true"/>
					<xs:attribute ref="xml:lang" use="optional"/>
				</xs:extension>
			</xs:complexContent>
		</xs:complexType>
	</xs:element>
	<xs:element name="request">
		<xs:annotation>
			<xs:documentation>A request to perform a certain action within the context of a
session previously established using open, along with parameters and other information affecting
the behavior of this request, consistent with the contract defined by the corresponding query-
harness.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:complexContent>
				<xs:extension base="h:requestType">
					<xs:sequence>
						<xs:element name="timestamp" type="xs:dateTime" minOccurs="0"/>
						<xs:element name="action" type="h:action"/>
					</xs:sequence>
					<xs:attribute name="session" type="xs:Name" use="required"/>
				</xs:extension>
			</xs:complexContent>
		</xs:complexType>
	</xs:element>
	<xs:element name="cancel">
		<xs:annotation>
			<xs:documentation>A request to cancel an action previously requested using a
request</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="timestamp" type="xs:dateTime" minOccurs="0"/>
			</xs:sequence>
			<xs:attribute name="session" type="xs:Name" use="required"/>
			<xs:attribute name="requestId" type="xs:string" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="progress">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="totalWork" type="xs:int"/>
				<xs:element name="remainingWork" type="xs:int"/>
				<xs:element name="status" type="xs:string" minOccurs="0"/>
				<xs:element name="timeRemaining" type="xs:duration" minOccurs="0"/>
				<xs:element name="timestamp" type="xs:dateTime" minOccurs="0"/>
			</xs:sequence>
			<xs:attribute name="session" type="xs:Name" use="required"/>
			<xs:attribute name="requestId" type="xs:string" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="response">
		<xs:annotation>
			<xs:documentation>A response returned corresponding to a request previously made.
The response, if populated, will conform to the details defined in the contract established via
query-harness for the action from the request.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="result" type="h:result"/>
				<xs:element name="message" type="xs:string" minOccurs="0"/>
				<xs:element name="duration" type="xs:float" minOccurs="0"/>
				<xs:element name="item" type="h:responseItem" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="xmlItem" type="h:responseXmlItem" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="file" type="h:fileItem" minOccurs="0" maxOccurs="unbounded"/>
				<xs:element name="group" type="h:responseGroup" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="timestamp" type="xs:dateTime" minOccurs="0"/>
			</xs:sequence>
			<xs:attribute name="session" type="xs:Name" use="required"/>
			<xs:attribute name="requestId" type="xs:string" use="optional"/>
			<xs:attribute ref="xml:lang" use="optional"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="event">
		<xs:annotation>
			<xs:documentation>An autonomous message issued by the provider of a harness to the
originator of one of its sessions informing it about some event that has occurred. It may also
contain additional information that must conform to the corresponding information in the query-
harness for the corresponding event.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="timestamp" type="xs:dateTime"/>
				<xs:element name="item" type="h:responseItem" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="xmlItem" type="h:responseXmlItem" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="file" type="h:fileItem" minOccurs="0" maxOccurs="unbounded"/>
				<xs:element name="group" type="h:responseGroup" minOccurs="0"
maxOccurs="unbounded"/>
			</xs:sequence>
			<xs:attribute name="session" type="xs:Name" use="required"/>
			<xs:attribute name="harness" type="h:namespace" use="required"/>
			<xs:attribute name="name" type="xs:Name" use="required"/>
			<xs:attribute ref="xml:lang" use="optional"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="close">
		<xs:annotation>
			<xs:documentation>A request to close a session previously opened using
open.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="timestamp" type="xs:dateTime" minOccurs="0"/>
			</xs:sequence>
			<xs:attribute name="session" type="xs:Name" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="notify-close">
		<xs:annotation>
			<xs:documentation>A notification from the provider that the session has
closed</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="timestamp" type="xs:dateTime" minOccurs="0"/>
			</xs:sequence>
			<xs:attribute name="session" type="xs:Name" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="notify-action">
		<xs:annotation>
			<xs:documentation>Notification of information about an action that was performed and
the response it produced.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="action" type="h:action"/>
				<xs:element name="started" type="xs:dateTime"/>
				<xs:element name="context" type="h:context" minOccurs="0"/>
				<xs:element name="requestParameter" type="h:requestParameter" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="requestXmlParameter" type="h:requestXmlParameter" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="requestFile" type="h:requestFile" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="requestGroup" type="h:requestGroup" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="result" type="h:result"/>
				<xs:element name="message" type="xs:string" minOccurs="0"/>
				<xs:element name="duration" type="xs:float" minOccurs="0"/>
				<xs:element name="responseItem" type="h:responseItem" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="responseXmlItem" type="h:responseXmlItem" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="responseFile" type="h:fileItem" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="responseGroup" type="h:responseGroup" minOccurs="0"
maxOccurs="unbounded"/>
				<xs:element name="timestamp" type="xs:dateTime" minOccurs="0"/>
			</xs:sequence>
			<xs:attribute name="session" type="xs:Name" use="required"/>
			<xs:attribute ref="xml:lang" use="optional"/>
		</xs:complexType>
	</xs:element>
	<!-- The remainder of the file contains definitions for classes referenced above -->
	<xs:complexType name="harnessDecl">
		<xs:annotation>
			<xs:documentation>Information about a named set of actions and events that may
describe a harness</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:elementDecl">
				<xs:sequence>
					<xs:element name="supercedes" type="h:namespace" minOccurs="0"/>
					<xs:element name="author" type="xs:string"/>
					<xs:element name="actionDecl" minOccurs="0" maxOccurs="unbounded">
						<xs:complexType>
							<xs:complexContent>
								<xs:extension base="h:namedElementDecl">
									<xs:sequence>
										<xs:element name="parameter" type="h:parameterDecl" minOccurs="0" maxOccurs="unbounded"/>
										<xs:element name="xmlParameter" type="h:xmlParameterDecl" minOccurs="0" maxOccurs="unbounded"/>
										<xs:element name="file" type="h:fileDecl" minOccurs="0" maxOccurs="unbounded"/>
										<xs:element name="group" type="h:requestGroupDecl" minOccurs="0" maxOccurs="unbounded"/>
										<xs:element name="responseDecl" type="h:responseDecl" minOccurs="0"/>
									</xs:sequence>
								</xs:extension>
							</xs:complexContent>
						</xs:complexType>
					</xs:element>
					<xs:element name="eventDecl" minOccurs="0" maxOccurs="unbounded">
						<xs:complexType>
							<xs:complexContent>
								<xs:extension base="h:responseDecl">
									<xs:sequence>
										<xs:element name="description" type="xs:string"/>
									</xs:sequence>
									<xs:attribute name="name" type="xs:Name" use="required"/>
								</xs:extension>
							</xs:complexContent>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
				<xs:attribute ref="xml:lang" use="required"/>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="responseDecl">
		<xs:annotation>
			<xs:documentation>A declaration of the information that will be returned in response
to a request for a given action supported by the harness (or harness including support for the
given sub)</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="item" type="h:responseItemDecl" minOccurs="0"
maxOccurs="unbounded"/>
			<xs:element name="xmlItem" type="h:responseXmlItemDecl" minOccurs="0"
maxOccurs="unbounded"/>
			<xs:element name="fileItem" type="h:fileItemDecl" minOccurs="0"
maxOccurs="unbounded"/>
			<xs:element name="group" type="h:responseGroupDecl" minOccurs="0"
maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="requestType">
		<xs:annotation>
			<xs:documentation>Information that is included along with a request that is made in
the context of a session using a given harness. This information needs to conform to the
contract laid out in the query-harness for the corresponding action.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="context" type="h:context" minOccurs="0"/>
			<xs:element name="parameter" type="h:requestParameter" minOccurs="0"
maxOccurs="unbounded"/>
			<xs:element name="xmlParameter" type="h:requestXmlParameter" minOccurs="0"
maxOccurs="unbounded"/>
			<xs:element name="file" type="h:requestFile" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="group" type="h:requestGroup" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="parameterDecl">
		<xs:annotation>
			<xs:documentation>Declaration of the information that may be used to describe a
certain parameter associated with a certain action declaration on a certain harness
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:namedElementDeclWithMandatory">
				<xs:sequence>
					<xs:element name="datatype" type="h:dataType" default="string"
minOccurs="0"/>
					<xs:element name="units" type="xs:string" minOccurs="0"/>
					<xs:element name="default" type="xs:string" minOccurs="0"/>
					<xs:element name="masked" type="xs:boolean" default="false" minOccurs="0"/>
					<xs:element name="isMultiline" type="xs:boolean" default="false"
minOccurs="0"/>
					<xs:element name="allowedValue" type="h:allowedValueDecl" minOccurs="0"
maxOccurs="unbounded"/>
					<xs:element name="allowedLength" type="h:allowedCountDecl" minOccurs="0"/>
					<xs:element name="allowedCount" type="h:allowedCountDecl" minOccurs="0"/>
					<xs:element name="allowedPattern" type="xs:string" minOccurs="0"
maxOccurs="unbounded"/>
					<xs:element name="allowedRange" type="h:allowedRangeDecl" minOccurs="0"
maxOccurs="unbounded"/>
					<xs:element name="enablementValue" type="h:enablementValueDecl"
minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="xmlParameterDecl">
		<xs:annotation>
			<xs:documentation>Declaration of the information that may be used to describe a
certain XML parameter -- i.e., a document fragment that may be included as part of a request to
perform a given action</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:namedElementDeclWithMandatory">
				<xs:sequence>
					<xs:element name="element" type="xs:Name"/>
					<xs:element name="xmlNamespace" type="xs:string"/>
					<xs:element name="enablementValue" type="h:enablementValueDecl"
minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="fileDecl">
		<xs:annotation>
			<xs:documentation>Declaration of the information that may be used to describe a file
that may be &quot;attached&quot; to the corresponding action request</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:namedElementDeclWithMandatory">
				<xs:sequence>
					<xs:element name="allowedCount" type="h:allowedCountDecl" minOccurs="0"/>
					<xs:element name="allowedFileExtension" type="xs:string" minOccurs="0"
maxOccurs="unbounded"/>
					<xs:element name="enablementValue" type="h:enablementValueDecl"
minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="allowedValueDecl">
		<xs:simpleContent>
			<xs:extension base="xs:string">
				<xs:attribute name="label" type="xs:string"/>
			</xs:extension>
		</xs:simpleContent>
	</xs:complexType>
	<xs:complexType name="allowedRangeDecl">
		<xs:sequence>
			<xs:element name="min" type="xs:decimal" minOccurs="0"/>
			<xs:element name="max" type="xs:decimal" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="allowedCountDecl">
		<xs:sequence>
			<xs:element name="min" type="xs:int" minOccurs="0"/>
			<xs:element name="max" type="xs:int" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="enablementValueDecl">
		<xs:sequence>
			<xs:element name="parameter" type="xs:Name"/>
			<xs:element name="value" type="xs:string"/>
			<xs:element name="enableOn" type="h:enableOnType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="requestGroupDecl">
		<xs:annotation>
			<xs:documentation>Declaration of a named group of parameters that may be included in
a request for a certain action</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:namedElementDeclWithMandatory">
				<xs:sequence>
					<xs:element name="allowedCount" type="h:allowedCountDecl" minOccurs="0"/>
					<xs:element name="parameterKeyName" type="xs:Name" minOccurs="0"/>
					<xs:element name="parameter" type="h:parameterDecl" minOccurs="0"
maxOccurs="unbounded"/>
					<xs:element name="group" type="h:requestGroupDecl" minOccurs="0"
maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="responseItemDecl">
		<xs:annotation>
			<xs:documentation>Declaration of a certain specific item of information that may be
returned in the response to a certain action</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:namedElementDeclWithMandatory">
				<xs:sequence>
					<xs:element name="default" type="xs:string" minOccurs="0"/>
					<xs:element name="datatype" type="h:dataType" default="string"
minOccurs="0"/>
					<xs:element name="units" type="xs:string" minOccurs="0"/>
					<xs:element name="masked" type="xs:boolean" default="false" minOccurs="0"/>
					<xs:element name="isMultiline" type="xs:boolean" default="false"
minOccurs="0"/>
					<xs:element name="allowedValue" type="h:allowedValueDecl" minOccurs="0"
maxOccurs="unbounded"/>
					<xs:element name="allowedCount" type="h:allowedCountDecl" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="responseXmlItemDecl">
		<xs:annotation>
			<xs:documentation>Declaration of a certain named XML document fragment that may be
returned in the response to a certain action</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:namedElementDeclWithMandatory">
				<xs:sequence>
					<xs:element name="element" type="xs:Name"/>
					<xs:element name="xmlNamespace" type="xs:string"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="fileItemDecl">
		<xs:annotation>
			<xs:documentation>Declaration of a certain named file that may be returned in the
response to a certain action</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:namedElementDeclWithMandatory">
				<xs:sequence>
					<xs:element name="allowedCount" type="h:allowedCountDecl" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="responseGroupDecl">
		<xs:annotation>
			<xs:documentation>Declaration of a certain named group of items that may be returned
in the response to a certain action</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="h:namedElementDeclWithMandatory">
				<xs:sequence>
					<xs:element name="allowedCount" type="h:allowedCountDecl" minOccurs="0"/>
					<xs:element name="itemKeyName" type="xs:Name" minOccurs="0"/>
					<xs:element name="item" type="h:responseItemDecl" minOccurs="0"
maxOccurs="unbounded"/>
					<xs:element name="group" type="h:responseGroupDecl" minOccurs="0"
maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="responseItem">
		<xs:simpleContent>
			<xs:extension base="xs:string">
				<xs:attribute name="name" type="xs:Name" use="required"/>
			</xs:extension>
		</xs:simpleContent>
	</xs:complexType>
	<xs:complexType name="fileItem">
		<xs:sequence>
			<xs:element name="filename" type="xs:string"/>
		</xs:sequence>
		<xs:attribute name="name" type="xs:Name" use="required"/>
	</xs:complexType>
	<xs:complexType name="responseXmlItem">
		<xs:sequence>
			<xs:any processContents="skip"/>
		</xs:sequence>
		<xs:attribute name="name" type="xs:Name" use="required"/>
	</xs:complexType>
	<xs:complexType name="responseGroup">
		<xs:sequence>
			<xs:element name="item" type="h:responseItem" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="group" type="h:responseGroup" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
		<xs:attribute name="name" type="xs:Name" use="required"/>
	</xs:complexType>
	<xs:complexType name="action">
		<xs:simpleContent>
			<xs:extension base="h:actionName">
				<xs:attribute name="harness" type="h:namespace" use="required"/>
			</xs:extension>
		</xs:simpleContent>
	</xs:complexType>
	<xs:complexType name="context">
		<xs:sequence>
			<xs:element name="details" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:any processContents="skip" minOccurs="0" maxOccurs="unbounded"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="context" type="h:context" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="entity" type="h:jid" use="optional"/>
		<xs:attribute name="harness" type="xs:string" use="optional"/>
		<xs:attribute name="session" type="xs:Name" use="optional"/>
		<xs:attribute name="requestId" type="xs:string" use="optional"/>
	</xs:complexType>
	<xs:complexType name="requestParameter">
		<xs:simpleContent>
			<xs:extension base="xs:string">
				<xs:attribute name="name" type="xs:Name" use="required"/>
			</xs:extension>
		</xs:simpleContent>
	</xs:complexType>
	<xs:complexType name="requestXmlParameter">
		<xs:sequence>
			<xs:any processContents="skip"/>
		</xs:sequence>
		<xs:attribute name="name" type="xs:Name" use="required"/>
	</xs:complexType>
	<xs:complexType name="requestFile">
		<xs:sequence>
			<xs:element name="filename" type="xs:string"/>
		</xs:sequence>
		<xs:attribute name="name" type="xs:Name" use="required"/>
	</xs:complexType>
	<xs:complexType name="requestGroup">
		<xs:sequence>
			<xs:element name="parameter" type="h:requestParameter" minOccurs="0"
maxOccurs="unbounded"/>
			<xs:element name="group" type="h:requestGroup" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
		<xs:attribute name="name" type="xs:Name" use="required"/>
	</xs:complexType>
	<xs:simpleType name="enableOnType">
		<xs:restriction base="xs:string">
			<xs:enumeration value="equal"/>
			<xs:enumeration value="not_equal"/>
			<xs:enumeration value="pattern_match"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="sessionMode">
		<xs:restriction base="xs:string">
			<xs:enumeration value="visible_and_interactive"/>
			<xs:enumeration value="visible_and_automated"/>
			<xs:enumeration value="invisible_and_automated"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="result">
		<xs:restriction base="xs:string">
			<xs:enumeration value="pass"/>
			<xs:enumeration value="fail"/>
			<xs:enumeration value="abort"/>
			<xs:enumeration value="pending"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="dataType">
		<xs:restriction base="xs:string">
			<xs:enumeration value="string"/>
			<xs:enumeration value="integer"/>
			<xs:enumeration value="decimal"/>
			<xs:enumeration value="boolean"/>
			<xs:enumeration value="anyURI"/>
			<xs:enumeration value="dateTime"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="jid">
		<xs:restriction base="xs:string"/>
	</xs:simpleType>
	<xs:simpleType name="namespace">
		<xs:restriction base="xs:anyURI"/>
	</xs:simpleType>
	<xs:simpleType name="actionName">
		<xs:restriction base="xs:Name"/>
	</xs:simpleType>
	<xs:complexType name="namedElementDeclWithMandatory">
		<xs:complexContent>
			<xs:extension base="h:namedElementDecl">
				<xs:sequence>
					<xs:element name="mandatory" type="xs:boolean" default="true" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="namedElementDecl">
		<xs:complexContent>
			<xs:extension base="h:elementDecl">
				<xs:attribute name="name" type="h:actionName" use="required"/>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="elementDecl">
		<xs:sequence>
			<xs:element name="label" type="xs:string"/>
			<xs:element name="tooltip" type="xs:string" minOccurs="0"/>
			<xs:element name="description" type="xs:string" minOccurs="0"/>
			<xs:element name="helpURI" type="xs:anyURI" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>

